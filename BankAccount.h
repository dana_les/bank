#ifndef BANKACCOUNT
#define BANKACCOUNT

#include <iostream>
#include <string>

using std::string;

class BankAccount
{
protected:
	string _name;
	double _balance;

public:
	BankAccount(string name);
	string getName();
	double getBalance();
	virtual string getKind();
	virtual double withdraw(double amount);
	virtual void deposit(double amount);

};

#endif // !BANKACCOUNT