#include <iostream>
#include<string>
#include"BankAccount.h"
#include "LimitedBankAccount.h"
#include"OverDraftAccount.h"


using namespace std;

int main()
{
	int account = 0;
	int kind; // 0- BankAccount, 1-overDraftAccount, 3 - LimitBankAccount
	int choice;
	string name;
	double add;
	double amount;
	int i;
	BankAccount* BankAc[20];
	cout << "Welcome to MagshiBank,\n1 to add new account\n2 to make transaction from existing account\n3 to list all accounts, type and balance\n4 to exit\n";
	cin >> choice;
	while (account < 20 && choice != 4)
	{
		if (choice == 1)
		{
			cout << "\n0 - BankAccount\n1 - overDraftAccount\n2 - LimitBankAccount\n";
			cin >> kind;
			cout << "enter name: \n";
			cin >> name;
			if (kind == 0)
			{
				BankAc[account] = new BankAccount(name);
			}
			else if (kind == 1)
			{
				cout << "Enter max over draft (negative number)\n";
				cin >> add;
				BankAc[account] = new OverDraftAccount(add, name);
			}
			else
			{
				cout << "Enter limit: \n";
				cin >> add;
				BankAc[account] = new LimitedBankAccount(add, name);
			}
			account++;
		}
		else if (choice == 2)
		{
			cout << "Enter name:\n";
			cin >> name;
			i = 0;
			while (i < account && BankAc[i]->getName() != name)
			{
				i++;
			}
			if (i >= account)
			{
				cout << "No Such Account\n";
			}
			else if (BankAc[i]->getName() == name)
			{
				cout << "\nWelcome " << name << " !\n1 to deposit money\n2 to withdraw money\n3 to show current balance\n4 back to main menu\n";
				cin >> choice;
				while (choice != 4)
				{

					if (choice == 1)
					{
						cout << "\nEnter amount: ";
						cin >> amount;
						BankAc[i]->deposit(amount);
					}
					else if (choice == 2)
					{
						cout << "\nEnter amount: ";
						cin >> amount;
						cout << "balance now: "<< BankAc[i]->withdraw(amount)<<"\n";
					}
					else if (choice == 3)
					{
						cout << "current balance " << BankAc[i]->getBalance()<<"\n";
					}
					cout << "\nWelcome " << name << " !\n1 to deposit money\n2 to withdraw money\n3 to show current balance\n4 back to main menu\n";
					cin >> choice;

				}
		
			}

		}
		else if (choice == 3)
		{
			i = 0;
			while (i < account)
			{
				cout << "name: " << BankAc[i]->getName() << ", kind: " << BankAc[i]->getKind() << ", balance: " << BankAc[i]->getBalance() << "\n";
				i++;
			}
		}
		cout << "Welcome to MagshiBank,\n1 to add new account\n2 to make transaction from existing account\n3 to list all accounts, type and balance\n4 to exit\n";
		cin >> choice;
	}

	return 0;
}