#ifndef OVERDRAFTACCOUNT
#define OVERDRAFTACCOUNT

#include <iostream>
#include <string>
#include "BankAccount.h"

using std::string;

class OverDraftAccount: public BankAccount
{

private:
	double _maxOverDraft;

public:
	OverDraftAccount(double max, string name);
	double getMax();
	virtual string getKind();
	virtual double withdraw(double amount);
	virtual void deposit(double amount);


};

#endif // !BANKACCOUNT
