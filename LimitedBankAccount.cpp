#include <iostream>
#include <string>
#include "LimitedBankAccount.h"



LimitedBankAccount::LimitedBankAccount(double limited, string name) :_limit(limited), BankAccount(name){}


void LimitedBankAccount::setLimited(double new_limit)
{
	_limit = new_limit;
}

double LimitedBankAccount::getLimited()
{
	return(_limit);
}

double LimitedBankAccount::withdraw(double amount)
{
	if (_limit >= amount && _balance >= amount)
	{
		_balance -= amount;
	}
	return(_balance);
}

void LimitedBankAccount::deposit(double amount)
{
	_balance += amount;
}

string LimitedBankAccount::getKind()
{
	return ("LimitedBankAccount");
}