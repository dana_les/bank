#ifndef IMITESBANKACCOUNT

#include <iostream>
#include <string>
#include "BankAccount.h"

using std::string;

class LimitedBankAccount : public BankAccount
{


private:
	double _limit;

public:
	LimitedBankAccount(double limited, string name);
	void setLimited(double new_limit);
	double getLimited();
	virtual string getKind();
	virtual double withdraw(double amount);
	virtual void deposit(double amount);

};


#endif 

