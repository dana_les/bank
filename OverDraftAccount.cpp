#include <iostream>
#include <string>
#include "OverDraftAccount.h"



OverDraftAccount::OverDraftAccount(double max, string name) :_maxOverDraft(max), BankAccount(name){}

double OverDraftAccount::getMax()
{
	return _maxOverDraft;
}

string OverDraftAccount::getKind()
{
	return ("OverDraftAccount");
}

double OverDraftAccount::withdraw(double amount)
{
	if (_balance - amount >= _maxOverDraft)
	{
		_balance -= amount;
	}
	return(_balance);
}

void OverDraftAccount::deposit(double amount)
{
	_balance += amount;
}