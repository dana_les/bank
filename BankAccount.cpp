#include <iostream>
#include <string>
#include "BankAccount.h"


BankAccount::BankAccount(string name) :_name(name), _balance(0){};

string BankAccount::getName()
{
	return _name;
}

double BankAccount::getBalance()
{
	return _balance;
}

double BankAccount::withdraw(double amount)
{
	if (_balance >= amount)
	{
		_balance -= amount;
	}
	return(_balance);
}

string BankAccount::getKind()
{
	return ("BankAccount");
}

void BankAccount::deposit(double amount)
{
	_balance += amount;

}